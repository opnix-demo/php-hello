# PHP sample program

## Project Introduction

This project is a simple PHP program, directory structure：

```
.
├── README.md
├── env.php
├── index.php
└── phpinfo.php
```

## Project requirements

Must exist in the root directory `index.php` or `composer.json` file.

## Local test

**Simple test：**Execute in this directory `php -S 0.0.0.0:5000` start the server，access [http://localhost:5000](http://localhost:5000) That can preview the effect.
